package org.example.model;

import org.example.model.contract.HasName;
import org.example.model.contract.Rideable;

abstract public class Vehicle implements HasName, Rideable {
    @Override
    public void ride() {
        System.out.println("Preparing...");
        System.out.printf("Riding %s...\n", getName());
    }
}
