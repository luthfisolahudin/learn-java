package org.example.model.contract;

public interface Rideable {
    void ride();

    boolean canRide();
}
