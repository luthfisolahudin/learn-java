package org.example.model.contract;

public interface HasName {
    String getName();
}
