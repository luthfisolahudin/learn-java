package org.example.model;

public class Motorcycle extends Vehicle {
    Fuel fuel;

    public Motorcycle(Fuel fuel) {
        this.fuel = fuel;
    }

    @Override
    public String getName() {
        return "Motorcycle";
    }

    @Override
    public void ride() {
        super.ride();
        fuel.decrement();
    }

    @Override
    public boolean canRide() {
        return fuel.getValue() > 0;
    }

    public static class Fuel {
        int value;

        public Fuel(int value) {
            this.value = value;

            validate();
        }

        public int getValue() {
            return value;
        }

        public void decrement() {
            value--;

            validate();
        }

        protected void validate() {
            validateFuel();
        }

        protected void validateFuel() {
            if (value < 0) {
                throw new RuntimeException("Fuel could not below zero");
            }
        }
    }

}
