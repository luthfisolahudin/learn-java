package org.example.model;

import org.example.model.contract.HasName;
import org.example.model.contract.Rideable;

import java.util.ArrayList;
import java.util.List;

public class Person implements HasName {
    String name;
    List<Rideable> rides;

    public Person(String name) {
        this.name = name;
        this.rides = new ArrayList<>();
    }

    @Override
    public String getName() {
        return name;
    }

    public void addRide(Rideable vehicle) {
        rides.add(vehicle);
    }

    public void move() {
        for (Rideable ride : rides) {
            if (ride.canRide()) {
                ride.ride();
                return;
            }
        }

        System.out.println("Could not move :(");
    }
}
