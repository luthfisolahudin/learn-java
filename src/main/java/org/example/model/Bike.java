package org.example.model;

public class Bike extends Vehicle {
    @Override
    public String getName() {
        return "Bike";
    }

    @Override
    public boolean canRide() {
        return true;
    }
}
