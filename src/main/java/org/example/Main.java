package org.example;

import org.example.model.Bike;
import org.example.model.Motorcycle;
import org.example.model.Person;

public class Main {
    public static void main(String[] args) {
        Person david = new Person("David");
        david.addRide(new Motorcycle(new Motorcycle.Fuel(10)));
        david.addRide(new Bike());

        System.out.printf("Hello, %s!\n", david.getName());
        david.move();
    }
}
