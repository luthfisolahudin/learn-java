Contoh penerapan beberapa pattern dalam bahasa Java.

## Patterns
- Association
- Aggregation
- Composition
- Dependency
- Inheritance
- Polymorphisms
- Interface
- Abstraction
